package com.example.cocktails.model.models

data class DrinkDetails (
    val idDrink : String,
    val strDrink: String,
    val strDrinkThumb: String,
    val category: Category
        )
