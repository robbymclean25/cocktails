package com.example.cocktails.model.remote

import com.example.cocktails.model.models.Category
import com.example.cocktails.model.models.Drink
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.create
import retrofit2.http.GET
import retrofit2.http.Query
interface CocktailService {

    companion object {
        private const val BASE_URL = "https://www.thecocktaildb.com"
        private const val CATEGORYLIST_ENDPOINT = "api/json/v1/1/list.php?c=list"
        private const val DRINK_ENDPOINT = "api/json/v1/1/lookup.php"

        fun getInstance():CocktailService = Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .build()
            .create()

    }
    @GET(CATEGORYLIST_ENDPOINT)
    suspend fun getCategories(@Query("c")c: String = "list"): MutableList<Drink>


    @GET(DRINK_ENDPOINT)
    suspend fun getDrinkDetails(@Query("i") i: String)


}