package com.example.cocktails.model.models

data class DrinkA (
    val strAlcohol: String,
    val strCategory: String,
    val strDrink: String,
    val strDrinkThumb: String,
    val strGlass: String,
    val strInstructions: String,

        )
