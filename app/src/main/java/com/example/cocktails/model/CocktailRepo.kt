package com.example.cocktails.model
import android.util.Log
import com.example.cocktails.model.remote.CocktailService
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import kotlinx.coroutines.delay
object CocktailRepo {

    private val cocktailService = CocktailService.getInstance()

    suspend fun getCategories() = withContext(Dispatchers.IO){
        delay(1000)
        return@withContext cocktailService.getCategories()
    }
    suspend fun getDrinks(category: String) = withContext(Dispatchers.IO) {
        Log.e("CocktailRepo",category)
        return@withContext cocktailService.getDrinkDetails(category)
    }
    suspend fun getDrinkDetails(id: String) = withContext(Dispatchers.IO){
        Log.e("TAG","SELECT YOUR BOOZE")
        return@withContext cocktailService.getDrinkDetails(id)
    }
}