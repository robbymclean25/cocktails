package com.example.cocktails.model.models

data class Drinks (
    val drinks:List<DrinkDetails>,
    val category: Category
        )
