package com.example.cocktails.model.models

data class Category (
    val drinks: List<Drink>
)