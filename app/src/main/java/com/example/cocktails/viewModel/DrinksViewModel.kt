package com.example.cocktails.viewModel
import android.util.Log
import kotlinx.coroutines.launch
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.cocktails.model.CocktailRepo
import com.example.cocktails.model.models.DrinkListCache
import com.example.cocktails.view.DrinksFragment.DrinksState

class DrinksViewModel: ViewModel() {
    val TAG = "Drinks"

    private val cache = DrinkListCache
    private val repo = CocktailRepo
    private val _drinksState: MutableLiveData<DrinksState> = MutableLiveData(DrinksState())
    val drinksState: LiveData<DrinksState> get() = _drinksState

    fun getDrinks(category: String){
        viewModelScope.launch{
            val drinksCache = cache.retrieveCache()
            val lastCategory = cache.getLastCategory()

            if(lastCategory != category){
                Log.e(TAG, "RECIEVED CALL")
                makeAPICall(category)
            }else{
                _drinksState.value = DrinksState(drinksCache)
            }
        }
    }

    private suspend fun makeAPICall(category:String){
        with(_drinksState){
            value = DrinksState(isLoading = true)
            val result = repo.getDrinks(category)
            //value = DrinksState(drinks = result.drinks, isLoading = false)
            //cache.updateCache(result.drinks)
            cache.setLastCategory(category)
        }
    }
}