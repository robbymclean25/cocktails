package com.example.cocktails.viewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx. lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.cocktails.model.CocktailRepo
import com.example.cocktails.view.CategoryFragment.CategoryState
import com.example.cocktails.model.models.CategoryCache
import kotlinx.coroutines.launch

class CategoryViewModel (): ViewModel() {
    private val repo: CocktailRepo = CocktailRepo
    private val _categoryState: MutableLiveData<CategoryState> = MutableLiveData(CategoryState())
    val categoryState: LiveData<CategoryState> get() = _categoryState

    fun getCategories() {
        viewModelScope.launch {
            with(_categoryState) {
                val cache = CategoryCache.retrieveCache()

                if (cache.isEmpty()) {
                    makeAPICall()
                } else {
                    value = CategoryState(list = cache, isLoading = false)
                }
            }
        }
    }
    private suspend  fun makeAPICall(){
        with(_categoryState){
            value = CategoryState(isLoading = true)
            val result = repo.getCategories()
            value = CategoryState(list = result, isLoading = false)
            CategoryCache.updateCache(value!!.list)
        }

    }


}