package com.example.cocktails.viewModel
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.cocktails.model.CocktailRepo
import com.example.cocktails.view.DrinkFragment.DrinkState
import kotlinx.coroutines.launch

class DrinkViewModel: ViewModel() {
    val repo = CocktailRepo
    private val _drinkState: MutableLiveData<DrinkState> = MutableLiveData(DrinkState())
    val drinkState: LiveData<DrinkState> get() = _drinkState

    fun getDrinkDetails(id: String){
        viewModelScope.launch {
            with(_drinkState){
                value = DrinkState(isLoading = true)
                val result = repo.getDrinkDetails(id)
            }
        }
    }
}