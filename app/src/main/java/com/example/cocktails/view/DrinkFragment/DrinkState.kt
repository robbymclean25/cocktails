package com.example.cocktails.view.DrinkFragment
import com.example.cocktails.model.models.Drink
data class DrinkState (
 val drink: Drink? = null,
 var isLoading: Boolean = false
        )
