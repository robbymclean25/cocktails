package com.example.cocktails.view.DrinksFragment
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.TextView
import android.widget.ImageView
import androidx.navigation.Navigation
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.navigation.findNavController
import androidx.recyclerview.widget.RecyclerView
import coil.load
import com.example.cocktails.databinding.ItemDrinkviewBinding
import com.example.cocktails.model.models.DrinkDetails

class DrinksAdapter(): RecyclerView.Adapter<DrinksAdapter.DrinkViewHolder>() {
    var drinkList: List<DrinkDetails> = listOf()
    inner class DrinkViewHolder(val binding: ItemDrinkviewBinding) :
        RecyclerView.ViewHolder(binding.root) {
        var drinkName: TextView = binding.tvDrink
        var drinkImg: ImageView = binding.imgDrink
        //var wholeItem: ConstraintLayout = binding.layoutItem
        fun displayImage(image: String) {
            binding.imgDrink.load(image)
        }
    }
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DrinkViewHolder{
        return DrinkViewHolder(ItemDrinkviewBinding.inflate(LayoutInflater.from(parent.context)))
    }
    override fun onBindViewHolder(holder: DrinksAdapter.DrinkViewHolder, position: Int) {
        with(holder) {
            drinkName.text = drinkList[position].strDrink
            displayImage(drinkList[position].strDrinkThumb)
            val elements = listOf(drinkName, drinkImg)

        }
    }
    override fun getItemCount(): Int {
        return drinkList.size
    }
    fun setList(list: List<DrinkDetails>) {
        val oldSize = drinkList.size
        drinkList = list
        notifyItemRangeChanged(oldSize, drinkList.size)
    }




}




