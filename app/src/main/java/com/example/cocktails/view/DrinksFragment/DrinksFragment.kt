package com.example.cocktails.view.DrinksFragment
import com.example.cocktails.databinding.FragmentDrinksBinding
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.example.cocktails.viewModel.DrinksViewModel


import androidx.fragment.app.Fragment
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager

class DrinksFragment: Fragment() {
    lateinit var binding: FragmentDrinksBinding
    private val args by navArgs<DrinksFragmentArgs>()
    val viewModel by viewModels<DrinksViewModel>()
    private val adapter by lazy{
        DrinksAdapter()
    }
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View?{
        binding = FragmentDrinksBinding.inflate(layoutInflater)
        viewModel.getDrinks(args.category)
        binding.recyclerView.adapter = adapter
        binding.recyclerView.layoutManager = LinearLayoutManager(this@DrinksFragment.context)
        initObservers()
        return binding.root
    }
    private fun initObservers(){
        viewModel.drinksState.observe(viewLifecycleOwner){
           adapter.setList(it.drinks)
        }
    }
    override fun onViewCreated(view: View, savedInstanceState: Bundle?){
        binding.btnGoBack.setOnClickListener {
            val action = DrinksFragmentDirections.actionDrinksFragmentToCategoryFragment()
            findNavController().navigate(action)
        }
    }



}