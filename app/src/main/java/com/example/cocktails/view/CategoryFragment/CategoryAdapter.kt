package com.example.cocktails.view.CategoryFragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.Navigation
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.cocktails.R
import com.example.cocktails.model.models.Category
import com.example.cocktails.model.models.Drink


class CategoryAdapter(categories: List<Category>, function: (Any?) -> Unit) : RecyclerView.Adapter<CategoryAdapter.CategoryViewHolder>() {
    var categoryList: List<Drink> = listOf()
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CategoryViewHolder{
        val v = LayoutInflater.from(parent.context).inflate(R.layout.item_cateogry, parent,false)
        return CategoryViewHolder(v)
    }

    override fun onBindViewHolder(holder: CategoryViewHolder, position: Int){
        with(holder.categoryName){
            text = categoryList[position].strCategory
            setOnClickListener {
                val navController = Navigation.findNavController(holder.itemView)

            }
        }
    }
    inner class CategoryViewHolder(categoryView: View) :RecyclerView.ViewHolder(categoryView){
        var categoryName: TextView

        init{
            categoryName = categoryView.findViewById(R.id.tvCategories)
        }
    }
    override fun getItemCount(): Int{
        return categoryList.size
    }
    fun setData(list: List<Drink>){
        categoryList  = list
        notifyItemRangeChanged(0,categoryList.size)

    }

}