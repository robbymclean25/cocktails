package com.example.cocktails.view.DrinkFragment
import android.view.LayoutInflater
import android.view.ViewGroup
import android.os.Bundle
import android.view.View
import androidx.navigation.findNavController
import com.example.cocktails.R
import androidx.fragment.app.Fragment
import com.example.cocktails.databinding.FragmentDrinkBinding
import androidx.core.view.isVisible
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.navArgs
import com.example.cocktails.model.models.Drink
import coil.load
import com.example.cocktails.databinding.FragmentCategoryBinding
import com.example.cocktails.viewModel.DrinkViewModel

class DrinkFragment : Fragment() {
    private val args by navArgs<DrinkFragmentArgs>()
    private val viewModel by viewModels<DrinkViewModel>()
    lateinit var binding: FragmentDrinkBinding
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentDrinkBinding.inflate(layoutInflater)
        viewModel.getDrinkDetails(args.drinkID)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        with(binding) {
            viewModel.drinkState.observe(viewLifecycleOwner) { state ->
                if (state.isLoading) {
                    binding.progressBar.isVisible = true
                }else if (!state.isLoading){
                    binding.progressBar.isVisible = false
                }
                //tvDrinkName.text
            }
        }
    }
}
