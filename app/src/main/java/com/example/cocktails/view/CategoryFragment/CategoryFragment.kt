package com.example.cocktails.view.CategoryFragment
import com.example.cocktails.databinding.FragmentCategoryBinding
import android.view.ViewGroup
import android.view.View
import android.view.LayoutInflater
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import android.os.Bundle
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.cocktails.viewModel.CategoryViewModel

class CategoryFragment : Fragment() {
    lateinit var binding: FragmentCategoryBinding

    val viewModel: CategoryViewModel by viewModels<CategoryViewModel>()


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentCategoryBinding.inflate(layoutInflater)
        viewModel.getCategories()
        initViews()
        return binding.root

    }

     fun initViews() {
        with(binding){
            viewModel.categoryState.observe(viewLifecycleOwner){state->
                val categoryAdapter = CategoryAdapter(state.categories){strCategory ->
                    val action = CategoryFragmentDirections.actionCategoryFragmentToDrinkFragment(drinkID = "cocktail",drink = "mimosa")
                    findNavController().navigate(action)
                }
                rvCategories.adapter = categoryAdapter
                rvCategories.layoutManager = LinearLayoutManager(context)
            }
        }
    }
}