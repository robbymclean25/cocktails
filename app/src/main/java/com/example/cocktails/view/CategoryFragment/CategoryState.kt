package com.example.cocktails.view.CategoryFragment

import com.example.cocktails.model.models.Category
import com.example.cocktails.model.models.Drink

data class CategoryState(
    val list: MutableList<Drink> = mutableListOf(),
    var isLoading: Boolean = false,
    val categories: List<Category>

)
