package com.example.cocktails.view.DrinksFragment

import com.example.cocktails.model.models.Category
import com.example.cocktails.model.models.DrinkDetails

data class DrinksState (
    val drinks: List<DrinkDetails> = listOf(),
    val category: String? = null,
    val isLoading : Boolean = false
        )
